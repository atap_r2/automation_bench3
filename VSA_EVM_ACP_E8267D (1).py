"""
using Python to set values on M8190A and read EVM from running copy of 89601B sofwtare

with major help from Kevin Klug
Author: Gennady Farber

klug@google.com
"""
from datetime import datetime
#import matplotlib.pyplot as plt
import numpy as np
#import sys
import time
import csv
#import visa
import os
import keysight.command_expert as kt
os.chdir('C:\Users\gfarber\Google Drive\Gennady Test Data\Automation')
AMPL = np.linspace(-10,-3,8)
EVM = np.zeros(len(AMPL))
EVM_EQ = np.zeros(len(AMPL))
Pout=np.zeros(len(AMPL))
ACP_Ref=np.zeros(len(AMPL))
ACP_Lo=np.zeros(len(AMPL))
ACP_Up=np.zeros(len(AMPL))


for index, val in enumerate(AMPL): 
  EVM[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[0]
  EVM_EQ[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[1]
  Pout[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[2]
  ACP_Ref[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[3]
  ACP_Lo[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[4]
  ACP_Up[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[5]

fileName = 'QAM256_TGA2594_MMA273336_31GHz15_270MHz-%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, np.transpose([AMPL,EVM,EVM_EQ,Pout,ACP_Ref,ACP_Lo,ACP_Up]), delimiter=',')