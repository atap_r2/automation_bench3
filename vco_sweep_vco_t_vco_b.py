"""
MSB=LSB=NSB=0,
sweep vco_t and vco_b
record: Fosc, Power, DC voltages

klug@google.com
"""

from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import re
import time
#import csv
#import visa
import os
import keysight.command_expert as kt
import sys
sys.path.insert(0, 'C:\Mindspeed\MindspeedScripts\MindspeedControl')
from mono_spi import Mono_SPI
mono = Mono_SPI('COM6', cmd_delay=.2) # for Jun 6 Mindspeed image , 1 sec delay is needed, 0.5sec give wt_err
# for May 23 version, 0.1 sec delay is ok., .2sec is ok if iwl delay=10ms
def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)
    
def bin2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 2)

chip = 'B2'; # this is number on the board
Fmin = 1 ; # set up for max Fosc, MSB=NSB=LSB=0
os.chdir('C:\R2SiliconTest\Automation\SCPI_SEQ')
AMPL = np.linspace(-10,-3,8)
N=16;
N2=16
Foscl = np.zeros(N)
Powerl = np.zeros(N)
Foscn = np.zeros(N)
Powern = np.zeros(N)
Foscm = np.zeros((N,4))
Powerm = np.zeros((N,4))
vtop = np.zeros((N,4))
vcen = np.zeros((N,4))
vgnd = np.zeros((N,4))
### for MSB=16

if Fmin:
    CENTER=4.5e9
    SPAN=2000e6
    RESBW=10e3
    NOTE=' vco_k=0x4, MSB=31,NSB=31,LSB=16'
    SUB='Fmin'
else:
## for MSB=0, 
    CENTER=10.82e9
    SPAN=2000e6
    RESBW=10e3
    NOTE=' vco_k=0x4, MSB=0,NSB=0,LSB=0'
    SUB='Fmax'

#regVal = hex2dec('0x00048021')
#regHexW2 = '0x00000041'
#regHex = dec2hex(regVal)
# set spectrum analyzer to the right scales
kt.run_sequence('MXA_ip31',[],[1,CENTER,SPAN,RESBW])

## put the chip in default states, all components are powered off
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.send_file('C:\R2SiliconTest\CSR\\alogic_synOn_antestOn.spi')
mono.send_file('C:\R2SiliconTest\CSR\\syn_On.spi')
# measure VCO Osc Freq

# top node 1A1, center 22A,ground 1A1
# NSB=1
if Fmin:
    base='0xFFFC8021'
    base0=bin(hex2dec(base))
    base1='0xFFFC8121'
    base2='0xFFFC81A1'
    base3='0xFFFC83A1'
    regVal = hex2dec(base)
    regValW2 = '0x00000001'  # bias is zero    
else:
    #### NSB Sweep,5 bit
    base='0x00048021'
    base0=bin(hex2dec(base))
    base1='0x00048121'
    base2='0x000481A1'
    base3='0x000483A1'
    regVal = hex2dec(base)
    regValW2 = '0x00000000'  # bias is zero


mono.send_cmd('iwl 0x0002B000 '+base)
regHex = dec2hex(regVal)
os=0
# sweep 
for i in range(0,4):
    regValW2_t = hex2dec(regValW2) + 2**1*(i+os) # vco_t
    for x in range(0,16): # vco_b loop
        print 'Bias Sweep, vco_b ' + str(x) + '; vco_t=' + str(i)
        regValW2New = regValW2_t + 2**3 * x;
        regHexW2New = dec2hex(regValW2New)
        print regHexW2New        
        mono.send_cmd('iwl 0x0002B004 ' + regHexW2New)
        time.sleep(2)
        [Powerm[x,i],Foscm[x,i]] = kt.run_sequence('MXA_ip31',[],[1,CENTER,SPAN,RESBW])
        #[Powerm[i,x],Foscm[i,x]] = kt.run_sequence('MXA_ip31',[],[1,Foscm[i,x],100e6,.1e6])
        time.sleep(6)       
        regTop = dec2hex(int(base1,16)|int(base,16))
        mono.send_cmd('iwl 0x0002B000 ' + regTop)
        [vtop[x,i]] = kt.run_sequence('DMM25_VDC')
        regCenter = dec2hex(int(base2,16)|int(base,16))
        mono.send_cmd('iwl 0x0002B000 ' + regCenter)
        [vcen[x,i]] = kt.run_sequence('DMM25_VDC')
        regGnd= dec2hex(int(base3,16)|int(base,16))
        mono.send_cmd('iwl 0x0002B000 ' + regGnd)
        [vgnd[x,i]] = kt.run_sequence('DMM25_VDC')
        print Powerm[x,i],Foscm[x,i],regTop,regCenter,regGnd,'W2='+regHexW2New
    regVal = hex2dec(base)
    
    
i_x2= np.linspace(0,N-1,N);
i_x2.shape=(N,1)
#Foscm.shape=(N,4)
#Powerm.shape=(N,4)
OUT=np.concatenate([i_x2,Foscm,Powerm,vtop,vcen,vgnd],axis=1)

fileName = chip + '_vco_t_vco_bias_sweep_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, OUT, fmt='%16s', delimiter='\t',header='Freq vs vco_b\ncode\t' + str(os) + \
'\t '+ str(os+1) +'\t'+ str(os+2)+'\t' + str(os+3) + '\tPwr(dBm)0,1,2,3' + 'vcen0,1,2,3\tvtop0,1,2,3\tvgnd0,1,2,3',\
footer='Address 0x04 ' + regValW2 + NOTE)
# bring chip to default
#mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.close()

#os=os+16;# if the NSB is 0x10
### NSB plot
plt.figure(figsize=(7,8),dpi=100)
plt.subplot(4,1,1)
plt.plot(i_x2, Foscm[:,0],i_x2, Foscm[:,1],i_x2, Foscm[:,2],i_x2, Foscm[:,3])
plt.title('vco bias sweep'+'\n' + NOTE)
plt.legend(('vct_t=0x0','vco_t=0x1','vco_t=0x2','vco_t=0x3'),loc=0,fontsize=9)
plt.grid(which='major', axis='both')
plt.ylabel('Freq [Hz]')
plt.xlabel('vco_b [4bit]')
plt.subplot(4,1,2)
#plt.plot(i_x2, vtop[:,0],i_x2, vcen[:,0],i_x2, vgnd[:,0])
plt.grid(which='major', axis='both')
plt.plot(i_x2, vtop)
plt.ylabel('vtop[V]')
plt.title('vtop')
plt.legend(('vco_t=0x0','vco_t=0x1','vco_t=0x2','vco_t=0x3'),loc=4,fontsize=9)
plt.subplot(4,1,3)
plt.plot(i_x2, vcen)
plt.grid(which='major', axis='both')
plt.title('vcenter')
plt.ylabel('Vcenter[V]')
plt.legend(('vco_t=0x0','vco_t=0x1','vco_t=0x2','vco_t=0x3'),loc=4,fontsize=9)
plt.subplot(4,1,4)
plt.plot(i_x2, vgnd)

plt.title('vgnd')
#plt.plot(i_x2, vtop)
#plt.legend(('NSB=0x0','NSB=0x1','NSB=0x2','NSB=0x3'))
#plt.legend(('vtop','vcenter','vgnd'))
plt.legend(('vco_t=0x0','vco_t=0x1','vco_t=0x2','vco_t=0x3'),loc=4,fontsize=9)
plt.grid(which='major', axis='both')
#plt.ylabel('V')
plt.ylabel('Vgnd[V]')
plt.xlabel('vco_b [4bit]')
plt.savefig(re.sub(r'.txt','',fileName)+'_fosc_vs_vbias'+str(os)+SUB+'.jpg',dpi=300)

plt.figure()
plt.subplot(4,1,1)
plt.title('Power vs vco_b' + NOTE)
plt.grid(which='major', axis='both')
plt.plot(i_x2,Powerm[:,0])
plt.ylabel('Power [dBm]')
plt.text(8,-50,'vct_t=0x0')
plt.subplot(4,1,2)
plt.grid(which='major', axis='both')
plt.plot(i_x2,Powerm[:,1])
plt.ylabel('Power [dBm]')
plt.text(8,-50,'vct_t=0x1')
plt.subplot(4,1,3)
plt.grid(which='major', axis='both')
plt.plot(i_x2,Powerm[:,2])
plt.ylabel('Power [dBm]')
plt.text(8,-50,'vct_t=0x2')
plt.subplot(4,1,4)
plt.plot(i_x2,Powerm[:,3])
plt.ylabel('Power [dBm]')
plt.text(8,-50,'vct_t=0x3')
#,'vco_t=0x1','vco_t=0x2','vco_t=0x3'))
plt.grid(which='major', axis='both')
plt.xlabel('vco_b [4bit]')
plt.ylabel('Power [dBm]')
plt.savefig(re.sub(r'.txt','',fileName)+'_Power_vs_vco_b'+str(os)+SUB+'.jpg',dpi=300)
