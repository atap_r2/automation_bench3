"""
using Python to set values on M8190A and read EVM from running copy of 89601B sofwtare

with major help from Kevin Klug
Author: Gennady Farber

klug@google.com
"""

from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import re
import time
#import csv
#import visa
import os
import keysight.command_expert as kt
import sys
sys.path.insert(0, 'C:\Mindspeed\MindspeedScripts\MindspeedControl')
from mono_spi import Mono_SPI
mono = Mono_SPI('COM6', cmd_delay=0.1)

def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)

chip = 'B2'; # this is number on the board
os.chdir('C:\R2SiliconTest\Automation\SCPI_SEQ')
AMPL = np.linspace(-10,-3,8)
N=16;
N2=32
Foscl = np.zeros(N)
Powerl = np.zeros(N)
Foscn = np.zeros(N2)
Powern = np.zeros(N2)
Foscm = np.zeros((N2,4))
Powerm = np.zeros((N2,4))
#Pout=np.zeros(len(AMPL))
#ACP_Ref=np.zeros(len(AMPL))
#ACP_Lo=np.zeros(len(AMPL))
#ACP_Up=np.zeros(len(AMPL))
## put the chip in default states, all components are powered off
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.send_file('C:\R2SiliconTest\CSR\\alogic_synOn.spi')
mono.send_file('C:\R2SiliconTest\CSR\\syn_On.spi')
# measure VCO Osc Freq

CENTER=5.9e9
SPAN=200e6
RESBW=10e3
#regVal = hex2dec('0x00048021')
#regHexW2 = '0x00000041'
#regHex = dec2hex(regVal)

#### MSB Sweep,5 bit
regVal = hex2dec('0x00048021')  #LSB = 0
regValW2 = '0x00000046'
mono.send_cmd('iwl 0x0002B004 0x00000046')
regHex = dec2hex(regVal)
# set spectrum analyzer to the right scales
kt.run_sequence('MXA_ip31',[],[1,7e9,8e9,.1e6])
time.sleep(15)
os=0
for i in range(0,4):
    regVal = regVal + 2**28*(i+os)
    for x in range(0,32): 
        print 'NSB Sweep ' + str(x) + 'MSB=' + str(i) 
        regValNew = regVal + 2**23 * x;
        regHexNew = dec2hex(regValNew)
        mono.send_cmd('iwl 0x0002B000 ' + regHexNew)
        time.sleep(2)
        [Powerm[x,i],Foscm[x,i]] = kt.run_sequence('MXA_ip31',[],[1,9e9,4e9,.1e6])
        #[Powerm[i,x],Foscm[i,x]] = kt.run_sequence('MXA_ip31',[],[1,Foscm[i,x],100e6,.1e6])
        time.sleep(10)
        print Powerm[x,i],Foscm[x,i]
    regVal = hex2dec('0x00048021')
    
    
NOTE=' vco_b=0x8,vco_k=0x4,vco_t=0x3, LSB=0'
i_x2=np.linspace(0,N2-1,N2);
i_x2.shape=(N2,1)
#Foscm.shape=(N,4)
#Powerm.shape=(N,4)
OUT=np.concatenate([i_x2,Foscm,Powerm],axis=1)
fileName = chip + '_vco_freq_sweep_msb_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, OUT, fmt='%16s', delimiter='\t',header='Freq vs NSB\ncode\t' + str(os) + \
'\t '+ str(os+1) +'\t'+ str(os+2)+'\t' + str(os+3) + '\t\Pwr(dBm)',\
footer='Address 0x04 ' + regValW2 + NOTE)

# bring chip to default
#mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.close()

os=0# if the MSB is 0x10
### MSB plot
plt.figure()
plt.plot(i_x2, Foscm[:,0],i_x2, Foscm[:,1],i_x2, Foscm[:,2],i_x2, Foscm[:,3])
plt.title('Freq, MSB sweep '+'\n' + NOTE)
#plt.legend(('MSB=0x0','MSB=0x1','MSB=0x2','MSB=0x3'))
plt.legend(('MSB='+str(0+os),'MSB='+str(1+os),'MSB='+str(2+os),'MSB='+str(3+os)))
plt.grid(which='major', axis='both')
plt.ylabel('Freq [Hz]')
plt.xlabel('NSB code [5bit]')
plt.savefig(re.sub(r'.txt','',fileName)+'_fosc_vs_MSB_NSB_ovlp_os'+str(os)+'.jpg')

plt.figure()
plt.plot(Foscm[:,0],Powerm[:,0],Foscm[:,0],Powerm[:,1],Foscm[:,0],Powerm[:,2],Foscm[:,0],Powerm[:,3])
plt.title('Power vs Fosc, MSB sweep'+'\n' + NOTE)
plt.grid
plt.grid(which='major', axis='both')
plt.legend(('MSB='+str(0+os),'MSB='+str(1+os),'MSB='+str(2+os),'MSB='+str(3+os)))
plt.xlabel('Freq [Hz]')
plt.ylabel('Power [dBm]')
plt.savefig(re.sub(r'.txt','',fileName)+'_Power_vs_Fosc_NSB_ovlp_os'+str(os)+'.jpg')
