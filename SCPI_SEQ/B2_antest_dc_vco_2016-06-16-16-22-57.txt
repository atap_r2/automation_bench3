# VDD voltage at VCO and VDD current with VCO on and off
# index	VDD1P8VCO	I(VCC_0V9_AN)
          VCO-ON	      1.81027249	    -0.445752269
         VCO-OFF	      1.86636405	     -0.43919397
          ALL-ON	      1.79587022	    -0.475723485
         ALL-OFF	      1.85140504	    -0.470234293
