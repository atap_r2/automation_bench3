"""
using Python to set values on M8190A and read EVM from running copy of 89601B sofwtare

with major help from Kevin Klug
Author: Gennady Farber

klug@google.com
"""

from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import re
import time
#import csv
#import visa
import os
import keysight.command_expert as kt
import sys
sys.path.insert(0, 'C:\Mindspeed\MindspeedScripts\MindspeedControl')
from mono_spi import Mono_SPI
mono = Mono_SPI('COM6', cmd_delay=.2) # for Jun 6 Mindspeed image , 1 sec delay is needed, 0.5sec give wt_err
# for May 23 version, 0.1 sec delay is ok., .2sec is ok if iwl delay=10ms
def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)

chip = 'B2'; # this is number on the board
os.chdir('C:\R2SiliconTest\Automation\SCPI_SEQ')
AMPL = np.linspace(-10,-3,8)
N=16;
N2=32
Foscl = np.zeros(N)
Powerl = np.zeros(N)
Foscn = np.zeros(N)
Powern = np.zeros(N)
Foscm = np.zeros((N,4))
Powerm = np.zeros((N,4))

### for MSB=16
CENTER=5.905e9
SPAN=50e6
RESBW=10e3

## for MSB=0, 
CENTER=10.82e9
SPAN=170e6
RESBW=10e3
#regVal = hex2dec('0x00048021')
#regHexW2 = '0x00000041'
#regHex = dec2hex(regVal)
# set spectrum analyzer to the right scales
kt.run_sequence('MXA_ip31',[],[1,CENTER,SPAN,RESBW])

## put the chip in default states, all components are powered off
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.send_file('C:\R2SiliconTest\CSR\\alogic_synOn_antestOn.spi')
mono.send_file('C:\R2SiliconTest\CSR\\syn_On.spi')
# measure VCO Osc Freq

# top node 1A1, center 22A,ground 1A1
#### NSB Sweep,5 bit
base='0x00048021'
regVal = hex2dec(base)
regValW2 = '0x00000046'
mono.send_cmd('iwl 0x0002B004 0x00000046')
regHex = dec2hex(regVal)

os=0
for i in range(0,4):
    regVal = regVal + 2**23*(i+os)
    for x in range(0,16): 
        print 'LSB Sweep ' + str(x) + '; NSB=' + str(i)
        regValNew = regVal + 2**19 * x;
        regHexNew = dec2hex(regValNew)
        mono.send_cmd('iwl 0x0002B000 ' + regHexNew)
        time.sleep(2)
        [Powerm[x,i],Foscm[x,i]] = kt.run_sequence('MXA_ip31',[],[1,CENTER,SPAN,RESBW])
        #[Powerm[i,x],Foscm[i,x]] = kt.run_sequence('MXA_ip31',[],[1,Foscm[i,x],100e6,.1e6])
        time.sleep(5)
        #base1='0x00048121'
        #base1='0x000481A1'
        #base1='0x000483A1'
        print Powerm[x,i],Foscm[x,i]
    regVal = hex2dec(base)
    
    
i_x2= np.linspace(0,N-1,N);
i_x2.shape=(N,1)
#Foscm.shape=(N,4)
#Powerm.shape=(N,4)
OUT=np.concatenate([i_x2,Foscm,Powerm],axis=1)
NOTE=' vco_b=0x8,vco_k=0x4,vco_t=0x3, MSB=0'
fileName = chip + '_vco_freq_sweep_LSB_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, OUT, fmt='%16s', delimiter='\t',header='Freq vs LSB\ncode\t' + str(os) + \
'\t '+ str(os+1) +'\t'+ str(os+2)+'\t' + str(os+3) + '\t\Pwr(dBm)',\
footer='Address 0x04 ' + regValW2 + NOTE)
# bring chip to default
#mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.close()

#os=os+16;# if the NSB is 0x10
### NSB plot
plt.figure()
plt.plot(i_x2, Foscm[:,0],i_x2, Foscm[:,1],i_x2, Foscm[:,2],i_x2, Foscm[:,3])
plt.title('Freq, LSB sweep'+'\n' + NOTE)
#plt.legend(('NSB=0x0','NSB=0x1','NSB=0x2','NSB=0x3'))
plt.legend(('NSB='+str(0+os),'NSB='+str(1+os),'NSB='+str(2+os),'NSB='+str(3+os)))
plt.grid(which='major', axis='both')
plt.ylabel('Freq [Hz]')
plt.xlabel('LSB code [4bit]')
plt.savefig(re.sub(r'.txt','',fileName)+'_fosc_vs_NSB_LSB_ovlp_os'+str(os)+'.jpg',dpi=300)

plt.figure()
plt.plot(Foscm[:,0],Powerm[:,0],Foscm[:,0],Powerm[:,1],Foscm[:,0],Powerm[:,2],Foscm[:,0],Powerm[:,3])
plt.title('Power vs Fosc, NSB sweep'+'0x04 '+ regHex + '\n' + NOTE)
plt.grid
plt.grid(which='major', axis='both')
plt.legend(('NSB='+str(0+os),'NSB='+str(1+os),'NSB='+str(2+os),'NSB='+str(3+os)))
plt.xlabel('Freq [Hz]')
plt.ylabel('Power [dBm]')
plt.savefig(re.sub(r'.txt','',fileName)+'_Power_vs_Fosc_LSB_ovlp_os'+str(os)+'.jpg',dpi=300)
