"""
using Python to set values on M8190A and read EVM from running copy of 89601B sofwtare

with major help from Kevin Klug
Author: Gennady Farber

klug@google.com
"""

from datetime import datetime
#import matplotlib.pyplot as plt
import numpy as np
#import sys
import time
#import csv
#import visa
import os
import keysight.command_expert as kt
import sys
sys.path.insert(0, 'C:\Mindspeed\MindspeedScripts\MindspeedControl')
from mono_spi import Mono_SPI
mono = Mono_SPI('COM6', cmd_delay=0.1)

def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)

chip = 'A1'; # this is number on the board
os.chdir('C:\R2SiliconTest\Automation\SCPI_SEQ')
AMPL = np.linspace(-10,-3,8)
N=33;
vdc = np.zeros(N)
vdcVCO = np.zeros(8)
vac = np.zeros(N)
step  = 4
#Pout=np.zeros(len(AMPL))
#ACP_Ref=np.zeros(len(AMPL))
#ACP_Lo=np.zeros(len(AMPL))
#ACP_Up=np.zeros(len(AMPL))
## put the chip in default states, all components are powered off
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.send_file('C:\R2SiliconTest\CSR\\alogic_antesten.spi')
#mono.send_file('C:\R2SiliconTest\CSR\\bias_highI_antest.spi') # increase bias current
# measure hrc bias voltage
#for index, val in enumerate(AMPL):
regVal = hex2dec('0x00000001')
regHex = dec2hex(regVal)
mono.send_cmd('iwl 0x0002000C 0x00000006')
time.sleep(2)
[vdc[0]] = kt.run_sequence('DMM25_IDC'); # leakage current
for x in range(1,N): 
    print 'itest test point ' + str(x)
    mono.send_cmd('iwl 0x00020010 ' + regHex)
    regVal = regVal * 2;
    regHex = dec2hex(regVal) 
    time.sleep(1)
    [vdc[x]] = kt.run_sequence('DMM25_IDC')
#  EVM[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[0]
#  EVM_EQ[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[1]
#  Pout[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[2]
#  ACP_Ref[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[3]
#  ACP_Lo[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[4]
#  ACP_Up[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[5]
regVal = hex2dec('0x00000001')
regHex = dec2hex(regVal)
mono.send_file('C:\R2SiliconTest\CSR\\bias_nominalI_antest.spi') # increase bias current
vdc2 = np.zeros(N)
[vdc2[0]] = kt.run_sequence('DMM25_IDC'); # leakage current

for x in range(1,N): 
    print 'itest test point ' + str(x)
    mono.send_cmd('iwl 0x00020010 ' + regHex)
    regVal = regVal * 2;
    regHex = dec2hex(regVal)
    time.sleep(2)
    [vdc2[x]] = kt.run_sequence('DMM25_IDC')

i_x= np.linspace(0,N-1,N)-1;
fileName = chip + '_bias_itest_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
legend=np.array(['ic25synth',
'ic25fpq',
'ic25fpi',
'ic25fnq',
'ic25fni',
'ic25hcpq',
'ic25hcpi',
'ic25hcnq',
'ic25hcni',
'ic50comp',
'ic50dac',
'ic100clk',
'ic50buf',
'ic50latch',
'ic50ddac',
'ic50cas',
'ic50rfpreamp',
'ic50rfpa',
'ic50spare1',
'ic50rfbb',
'ic25spare1',
'ic25spare2',
'ic25cml',
'ic100_clk',
'ic100_latch',
'ic100_isf',
'ic100_tail',
'ic100_cas',
'ic100_sw',
'unused',
'unused',
'unused'])
# need to remove the leakage current on test bus
#np.savetxt(fileName, np.transpose([i_x, vdc-vdc[0]]), fmt='%.3e', delimiter='\t',header='itest in bias test point: col1:testpt,col2:I(uA)')
np.savetxt(fileName, np.transpose([i_x, vdc-vdc[0], vdc2-vdc2[0], -(vdc-vdc[0])+vdc2-vdc2[0]]), fmt='%.3e', delimiter='\t',header='itest in bias current test point:\n col1:testpt,col2:def, col3:nominal, col3-col2')

mono.close()

