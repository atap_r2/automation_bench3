"""
using Python to set values on M8190A and read EVM from running copy of 89601B sofwtare

with major help from Kevin Klug
Author: Gennady Farber

klug@google.com
"""

from datetime import datetime
#import matplotlib.pyplot as plt
import numpy as np
import time
#import csv
#import visa
import os
import keysight.command_expert as kt
import sys
sys.path.insert(0, 'C:\Mindspeed\MindspeedScripts\MindspeedControl')
from mono_spi import Mono_SPI
mono = Mono_SPI('COM6')

def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)

#mono.send_cmd('rl 0x00')
chip = 'B2_VCC_AN_1.98V' ; # this is number on the board
os.chdir('C:\R2SiliconTest\Automation\SCPI_SEQ')
AMPL = np.linspace(-10,-3,8)
N=15;
vdc = np.zeros(N)
vdcVCO = np.zeros(8)
vac = np.zeros(N)
step  = 4
#Pout=np.zeros(len(AMPL))
#ACP_Ref=np.zeros(len(AMPL))
#ACP_Lo=np.zeros(len(AMPL))
#ACP_Up=np.zeros(len(AMPL))
## put the chip in default states, all components are powered off
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.send_file('C:\R2SiliconTest\CSR\\alogic_hrcOn_antestOn.spi')
# measure hrc bias voltage
#for index, val in enumerate(AMPL):
regVal = hex2dec('0x0030ffc4')

for x in range(0,N): 
    #(vdc[x],vac[x])=kt.run_sequence('DMM_DC_AC')
    regValNew = regVal + x * step
    regHex = dec2hex(regValNew)
    mono.send_cmd('IndirectWriteLong 0x00023004 ' + regHex)
    time.sleep(2)
    [vdc[x]] = kt.run_sequence('DMM25_VDC')
#  EVM[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[0]
#  EVM_EQ[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[1]
#  Pout[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[2]
#  ACP_Ref[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[3]
#  ACP_Lo[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[4]
#  ACP_Up[index] = kt.run_sequence('VSA_EVM_ACP_E8267D',[],[float(val)])[5]
SIM=np.array([.620,
1.16,
1.16,
1.16,
.850,
1.2,
.620,
.930,
.620,
1.16,
.620,
.620,
.930,
1.16,
1.16])
hrc_x= np.linspace(1,15,15)
fileName = chip + '_antest_dc_hrc_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, np.transpose([hrc_x, SIM, vdc]), fmt='%.3e', delimiter='\t', header='HRC test point.\ntestpt sim meas')

# measure VCO voltage thru antest
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.send_file('C:\R2SiliconTest\CSR\\alogic_SynOn_antestOn.spi')
#mono.send_file('C:\R2SiliconTest\CSR\\alogic_AllOn_antestOn.spi')
regVal = hex2dec('0x00048021') # is not connected to test bus
#regVal = hex2dec('0x00048020') # is not connected to test bus, VCO is turned off
step = 128
time.sleep(5)
for x in range(0,7): 
    #(vdc[x],vac[x])=kt.run_sequence('DMM_DC_AC')
    regValNew = regVal + x * step
    regHex = dec2hex(regValNew)
    print "#meas VCO DC volt test pt" + str(x)
    mono.send_cmd('IndirectWriteLong 0x0002B000 ' + regHex)
    time.sleep(2)
    [vdcVCO[x]] = kt.run_sequence('DMM25_VDC')
vco_x= np.linspace(0,7,8)
fileName = chip + '_antest_dc_vco_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, np.transpose([vco_x, vdcVCO]) , fmt='%.3e', delimiter='\t',header='vco test point.\ntestpt meas(v)')

mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
#time.sleep(10)
mono.close()
