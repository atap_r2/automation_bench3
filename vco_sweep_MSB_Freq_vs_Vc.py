"""
using Python to set values on M8190A and read EVM from running copy of 89601B sofwtare

with major help from Kevin Klug
Author: Gennady Farber

klug@google.com
"""

from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import re
import time
#import csv
#import visa
import os
import keysight.command_expert as kt
import sys
sys.path.insert(0, 'C:\Mindspeed\MindspeedScripts\MindspeedControl')
from mono_spi import Mono_SPI
mono = Mono_SPI('COM6', cmd_delay=0.1)

def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)

chip = 'B2'; # this is number on the board
os.chdir('C:\R2SiliconTest\Automation\SCPI_SEQ')
AMPL = np.linspace(-10,-3,8)
N=16;
N2=32
Foscl = np.zeros(N)
Powerl = np.zeros(N)
Foscn = np.zeros(N2)
Powern = np.zeros(N2)
Foscm = np.zeros((4,N2))
Powerm = np.zeros((4,N2))
VDC = np.zeros(N2)
#Pout=np.zeros(len(AMPL))
#ACP_Ref=np.zeros(len(AMPL))
#ACP_Lo=np.zeros(len(AMPL))
#ACP_Up=np.zeros(len(AMPL))
## put the chip in default states, all components are powered off
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.send_file('C:\R2SiliconTest\CSR\\alogic_synOn_antestOn.spi')
mono.send_file('C:\R2SiliconTest\CSR\\syn_On.spi')
# measure VCO Osc Freq

CENTER=5.9e9
SPAN=200e6
RESBW=10e3
#regVal = hex2dec('0x00048021')
#regHexW2 = '0x00000041'
#regHex = dec2hex(regVal)

#### MSB Sweep,5 bit
W2='0x00000046'
## this setting will turn analog test bus switch of VCO, connecting Vc to antest
regVal = hex2dec('0x0FFC80A1')
regValW2 = hex2dec(W2)
#mono.send_cmd('iwl 0x0002B004 0x00000040')
regHex = dec2hex(regVal)
# set spectrum analyzer to the right scales
kt.run_sequence('MXA_ip31',[],[1,7.5e9,8e9,1e6])
time.sleep(5)
for i in range(0,1):
    regValW2 = regValW2 + 2**3*i
    for m in range(0,2):
        regValW2New = regValW2 + m;
        regHexW2New = dec2hex(regValW2New)
        mono.send_cmd('iwl 0x0002B004 ' + regHexW2New)
        for x in range(0,16): 
            print 'MSB Sweep ' + str(m*16+x) + '; i=' + str(i)
            regValNew = regVal + 2**28 * x;
            regHexNew = dec2hex(regValNew)
            mono.send_cmd('iwl 0x0002B000 ' + regHexNew)
            time.sleep(.5)
            [Powerm[i,x+m*16],Foscm[i,x+m*16]] = kt.run_sequence('MXA_ip31',[],[1,7.5e9,8e9,1e6])
            time.sleep(2)
            [VDC[x+m*16]] = kt.run_sequence('DMM25_VDC')
            print Powerm[i,x+m*16],Foscm[i,x+m*16]
    regValW2 = hex2dec(W2)
    
    
i_x2= np.linspace(0,N2-1,N2);
fileName = chip + '_vco_freq_sweep_msb_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, np.transpose([i_x2,Foscm,Powerm]), fmt='%16s', delimiter='\t',header='Freq vs MSB\ncode\tFosc(Hz)\t\Pwr(dBm)',\
footer='Address 0x04 ' + regHexW2New)
# bring chip to default
#mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.close()


### MSB plot
plt.figure()
plt.subplot(2,1,1)
plt.plot(i_x2, Foscm[0,:],'-o')
plt.title('Freq, MSB sweep'+'0x04 '+ regHexW2New)
#plt.legend(('Bias=8','Bias=9','Bias=10','Bias=11'))
plt.ylabel('Freq [Hz]')
plt.xlabel('MSB code [5bit]')
plt.savefig(re.sub(r'.txt','',fileName)+'_Fosc_vs_MSB.jpg')

#plt.figure()
plt.subplot(2,1,2)
plt.plot(Foscm[0,:],VDC)
plt.title('Fosc vs Vc, MSB Sweep'+'0x04 '+ regHexW2New)
plt.grid
#plt.legend(('Bias=8','Bias=9','Bias=10','Bias=11'))
plt.xlabel('Freq [Hz]')
plt.ylabel('Vc [V]')
plt.savefig(re.sub(r'.txt','',fileName)+'_Fosc_vc.jpg')
