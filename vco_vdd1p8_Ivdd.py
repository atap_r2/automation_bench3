"""
Use python to DMM and program mono 1.0 chip

Author: Bill Tsang

billtsang@google.com
"""

from datetime import datetime
#import matplotlib.pyplot as plt
import numpy as np
#import sys
import time
import csv
#import visa
import os
import keysight.command_expert as kt
import sys
sys.path.insert(0, 'C:\Mindspeed\MindspeedScripts\MindspeedControl')
from mono_spi import Mono_SPI
mono = Mono_SPI('COM6',cmd_delay=0.1)

def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)

#mono.send_cmd('rl 0x00')
chip = 'B2' ; # this is number on the board
os.chdir('C:\R2SiliconTest\Automation\SCPI_SEQ')
#AMPL = np.linspace(-10,-3,8)
N=4;
vdc = np.zeros(N)
idc = np.zeros(N)

#Pout=np.zeros(len(AMPL))
#ACP_Ref=np.zeros(len(AMPL))
#ACP_Lo=np.zeros(len(AMPL))
#ACP_Up=np.zeros(len(AMPL))
## put the chip in default states, all components are powered off
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
# turn top antest switch
mono.send_file('C:\R2SiliconTest\CSR\\alogic_SynOn_antestOn.spi')
mono.send_file('C:\R2SiliconTest\CSR\syn_On.spi')
[vdc[0]] = kt.run_sequence('DMM25_VDC')
[idc[0]] = kt.run_sequence('DMM24_IDC_10A')
mono.send_file('C:\R2SiliconTest\CSR\syn_Off.spi')
[vdc[1]] = kt.run_sequence('DMM25_VDC')
[idc[1]] = kt.run_sequence('DMM24_IDC_10A')

# turn everything on
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.send_file('C:\R2SiliconTest\CSR\\alogic_AllOn_antestOn.spi')
mono.send_file('C:\R2SiliconTest\CSR\syn_On.spi')
[vdc[2]] = kt.run_sequence('DMM25_VDC')
[idc[2]] = kt.run_sequence('DMM24_IDC_10A')
mono.send_file('C:\R2SiliconTest\CSR\syn_Off.spi')
[vdc[3]] = kt.run_sequence('DMM25_VDC')
[idc[3]] = kt.run_sequence('DMM24_IDC_10A')
legend = np.array(['VCO-ON','VCO-OFF','ALL-ON','ALL-OFF'])
# make it a column array
legend.shape = (N,1)
vdc.shape = (N,1)
idc.shape = (N,1)
out = np.concatenate([legend,vdc,idc],axis=1)
fileName = chip + '_antest_dc_vco_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, out, fmt='%16s', delimiter='\t', \
 header='VDD voltage at VCO and VDD current with VCO on and off\nindex\tVDD1P8VCO\tI(VCC_1V8_AN)', \
 footer='VDD 1.8V Jumper at +10% location, VDD1P8=1.98V')
#time.sleep(10)
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
I1P8_VCO = idc[1]-idc[0]
Vdiff = vdc[1]-vdc[0]

if 1:
    #fileName = chip + 'isupply_dc_vco_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
    F = open(fileName,'a')
    F.write('VCO current from 1.8V Supply '+str(I1P8_VCO))
    F.write('\nmeas Voltage from 1.8V Supply '+str(Vdiff))
    F.write('\n1.8V Supply Routing Resistance' + str(Vdiff/I1P8_VCO))
    F.close()

mono.close()
