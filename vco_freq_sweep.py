"""
using Python to set values on M8190A and read EVM from running copy of 89601B sofwtare

with major help from Kevin Klug
Author: Gennady Farber

klug@google.com
"""

from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import re
import time
#import csv
#import visa
import os
import keysight.command_expert as kt
import sys
sys.path.insert(0, 'C:\Mindspeed\MindspeedScripts\MindspeedControl')
from mono_spi import Mono_SPI
mono = Mono_SPI('COM6', cmd_delay=0.1)

def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)

chip = 'B2'; # this is number on the board
os.chdir('C:\R2SiliconTest\Automation\SCPI_SEQ')
AMPL = np.linspace(-10,-3,8)
N=16;
N2=32
Foscl = np.zeros(N)
Powerl = np.zeros(N)
Foscn = np.zeros(N2)
Powern = np.zeros(N2)
Foscm = np.zeros(N2)
Powerm = np.zeros(N2)
#Pout=np.zeros(len(AMPL))
#ACP_Ref=np.zeros(len(AMPL))
#ACP_Lo=np.zeros(len(AMPL))
#ACP_Up=np.zeros(len(AMPL))
## put the chip in default states, all components are powered off
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.send_file('C:\R2SiliconTest\CSR\\alogic_synOn.spi')
mono.send_file('C:\R2SiliconTest\CSR\\syn_On.spi')
# measure VCO Osc Freq

CENTER=5.9e9
SPAN=200e6
RESBW=10e3
regVal = hex2dec('0x00048021')
regHexW2 = '0x00000041'
regHex = dec2hex(regVal)
kt.run_sequence('MXA_ip31',[],[1,CENTER,150e6,RESBW])
time.sleep(5)
### LSB Sweep, 4bit
mono.send_cmd('iwl 0x0002B004 ' + regHexW2)
'''
for x in range(0,16): 
    print 'LSB Sweep ' + str(x)
    regValNew = regVal + 2**19 * x;
    regHexNew = dec2hex(regValNew)
    mono.send_cmd('iwl 0x0002B000 ' + regHexNew)
    mono.send_cmd('iwl 0x0002B004 ' + regHexW2)
    time.sleep(.5)
    [Powerl[x],Foscl[x]] = kt.run_sequence('MXA_ip31',[],[1,CENTER,150e6,RESBW])
    time.sleep(3)
    print [Powerl[x],Foscl[x]]    


kt.run_sequence('MXA_ip31',[],[1,5.8e9,300e6,RESBW])
time.sleep(5)
#### NSB Sweep,5 bit
regVal = hex2dec('0x007c8021')
regHex = dec2hex(regVal)
mono.send_cmd('iwl 0x0002B004 ' + regHexW2)
for x in range(0,32): 
    print 'NSB Sweep ' + str(x)
    regValNew = regVal + 2**23 * x;
    regHexNew = dec2hex(regValNew)
    mono.send_cmd('iwl 0x0002B000 ' + regHexNew)
    time.sleep(.5)
    [Powern[x],Foscn[x]] = kt.run_sequence('MXA_ip31',[],[1,5.8e9,300e6,RESBW])
    time.sleep(3)
    print [Powern[x],Foscn[x]]
'''
#### MSB Sweep,5 bit
regVal = hex2dec('0x0FFC8021')
regValW2 = hex2dec('0x00000044')
regValW2 = hex2dec('0x00000044')
#mono.send_cmd('iwl 0x0002B004 0x00000040')
regHex = dec2hex(regVal)
# set spectrum analyzer to the right scales
kt.run_sequence('MXA_ip31',[],[1,7.5e9,8e9,1e6])
time.sleep(5)
for m in range(0,2):
    regValW2New = regValW2 + m;
    regHexW2New = dec2hex(regValW2New)
    mono.send_cmd('iwl 0x0002B004 ' + regHexW2New)
    for x in range(0,16): 
        print 'MSB Sweep ' + str(x)
        regValNew = regVal + 2**28 * x;
        regHexNew = dec2hex(regValNew)
        mono.send_cmd('iwl 0x0002B000 ' + regHexNew)
        time.sleep(1)
        [Powerm[x+m*16],Foscm[x+m*16]] = kt.run_sequence('MXA_ip31',[],[1,7.5e9,8e9,1e6])
        time.sleep(2)
        print Powerm[x+m*16],Foscm[x+m*16]
        
i_x= np.linspace(0,N-1,N);
i_x2= np.linspace(0,N2-1,N2);
fileName1 = chip + '_vco_freq_sweep_lsb_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName1, np.transpose([i_x,Foscl,Powerl]), fmt='%.5e', delimiter='\t',header='Freq vs LSB\ncode\tFosc(Hz)\t\Pwr(dBm)',\
footer='Address 0x04 ' + regHexW2)
fileName2 = chip + '_vco_freq_sweep_nsb_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName2, np.transpose([i_x2,Foscn,Powern]), fmt='%.5e', delimiter='\t',header='Freq vs NSB\ncode\tFosc(Hz)\t\Pwr(dBm)',\
footer='Address 0x04 ' + regHexW2)
fileName3 = chip + '_vco_freq_sweep_msb_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName3, np.transpose([i_x2,Foscm,Powerm]), fmt='%.5e', delimiter='\t',header='Freq vs MSB\ncode\tFosc(Hz)\t\Pwr(dBm)',\
footer='Address 0x04 ' + regHexW2New)
# bring chip to default
#mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.close()


'''
### LSB plot
plt.figure()
plt.plot(i_x, Foscl,'-o')
plt.title('Freq, LSB sweep')
plt.ylabel('Freq [Hz]')
plt.xlabel('LSB code [4bit]')
plt.savefig(fileName1+'fosc_vs_LSB.jpg')
plt.figure()
plt.plot(Foscl,Powerl,'-o')
plt.title('Power vs Fosc, LSB sweep')
plt.xlabel('Freq [Hz]')
plt.ylabel('Power [dBm]')

plt.figure()
plt.plot(i_x2, Foscn,'-o')
plt.title('Freq, NSB sweep')
plt.ylabel('Freq [Hz]')
plt.xlabel('NSB code [5bit]')
plt.savefig(fileName2+'fosc_vs_NSB.jpg')
plt.figure()
plt.plot(Foscn,Powern,'-o')
plt.title('Power vs Fosc, NSB')
'''
### MSB plot
plt.figure()
plt.plot(i_x2, Foscm,'-o')
plt.title('Freq, MSB sweep'+'0x04 '+ regHexW2New)
plt.ylabel('Freq [Hz]')
plt.xlabel('MSB code [5bit]')
plt.savefig(re.sub(r'.txt','',fileName3)+'_fosc_vs_MSB.jpg')

plt.figure()
plt.plot(Foscm,Powerm,'-o')
plt.title('Power vs Fosc, MSB sweep'+'0x04 '+ regHexW2New)
plt.grid
plt.xlabel('Freq [Hz]')
plt.ylabel('Power [dBm]')
plt.savefig(re.sub(r'.txt','',fileName3)+'_Power_vs_Freq.jpg')
